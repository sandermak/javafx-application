package application;

import javafx.stage.Stage;

public class Main extends javafx.application.Application {

   public static void main(String... args) {
      launch(args);
   }

   @Override
   public void start(Stage primaryStage) {
     primaryStage.setTitle("Testing");
     primaryStage.show();
   }

}